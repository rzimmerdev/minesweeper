from __future__ import print_function
import random
import os


def cls():
    os.system('cls' if os.name == 'nt' else 'clear')


class Field:
    def __init__(self, m, l, c):
        self.mines = m
        self.n_lines = l
        self.n_collumns = c
        self.lines = []


class Line:
    def __init__(self, i):
        self.id = i
        self.squares = []


class Square:
    def __init__(self, i, l, c):
        self.id = i
        self.status = 'X'
        self.type = None
        self.b_number = 0
        self.pos = [l, c]


def create(f):
    for l in range(0, f.n_lines):
        new_line = Line(l)
        f.lines.append(new_line)
        for c in range(0, f.n_collumns):
            new_line.squares.append(Square((l*f.n_collumns + c), l, c))


def print_field(f):
    for l in range(0, f.n_lines):
        for c in range(0, f.n_collumns):
            if c == f.n_collumns - 1:
                print(f.lines[l].squares[c].status)
            else:
                print(f.lines[l].squares[c].status, end=" ")


def place_mines_on(f):
    max_id = f.n_lines*f.n_collumns - 1
    bombs_left = f.mines
    while bombs_left > 0:
        random_bomb_id = random.randint(0, max_id)
        r_line = int(random_bomb_id/f.n_collumns)
        r_collumn = random_bomb_id - f.n_collumns*r_line
        try:
            if f.lines[r_line].squares[r_collumn].type != 'bomb':
                f.lines[r_line].squares[r_collumn].type = 'bomb'
                bombs_left -= 1
        except:
            pass


def coord_in_range(f, l, c):
    if f.n_lines > l >= 0:
        if f.n_collumns > c >= 0:
            return True
        else:
            return False
    else:
        return False


def bad_input(f):
    os.system('cls' if os.name == 'nt' else 'clear')
    print_field(f)
    print("That's not a valid input!")


def get_adjacent(f, s_id):
    line = int(s_id / f.n_collumns)
    collumn = s_id - f.n_collumns * line
    adjacent = []
    for l in range(-1, 2):
        for c in range(-1, 2):
            try:
                if -1 < (line + l) < f.n_lines and -1 < (collumn + c) < f.n_collumns:
                    adjacent.append(f.lines[line + l].squares[collumn + c].id)
            except IndexError:
                pass
    adjacent.remove(f.lines[line].squares[collumn].id)
    return adjacent


def is_bomb(f, coords):
    if f.lines[coords[0] - 1].squares[coords[1] - 1].type == 'bomb':
        return True
    else:
        return False


def set_squares_type(f):
    for l in range(0, f.n_lines):
        for c in range(0, f.n_collumns):
            adjacent = get_adjacent(f, f.lines[l].squares[c].id)
            for s in range(0, len(adjacent)):
                line = int(adjacent[s] / f.n_collumns)
                collumn = adjacent[s] - f.n_collumns * line
                if f.lines[line].squares[collumn].type == 'bomb':
                    f.lines[l].squares[c].b_number += 1


def react_to_input(f, c):
    if c[2] == 'c' and f.lines[c[0] - 1].squares[c[1] - 1].status != 'marked':
        if not is_bomb(f, c):
            f.lines[c[0] - 1].squares[c[1] - 1].status = f.lines[c[0] - 1].squares[
                c[1] - 1].b_number
            print_field(f)
            return True
        else:
            return False
    elif c[2] == 'm':
        if f.lines[c[0] - 1].squares[c[1] - 1].status == 'M':
            f.lines[c[0] - 1].squares[c[1] - 1].status = 'X'
            print_field(f)
        else:
            try:
                int(f.lines[c[0] - 1].squares[c[1] - 1].status)
                print_field(f)
            except ValueError:
                f.lines[c[0] - 1].squares[c[1] - 1].status = 'M'
                print_field(f)
    return True


def start_game(mines, lines, collumns):
    os.system('cls' if os.name == 'nt' else 'clear')
    field = Field(mines, lines, collumns)
    create(field)
    place_mines_on(field)
    set_squares_type(field)
    print_field(field)
    in_game = True
    print('\n' + '\n' + 'Enter your desired square, according to this format: LINE COLLUMN c/m')
    print('With "c" being "click" and "m" being "mark as bomb".')
    while in_game:
        coord = input().split(' ')
        try:
            int(coord[0])
        except ValueError:
            bad_input(field)
            continue
        try:
            int(coord[1])
        except ValueError:
            bad_input(field)
            continue
        if coord[2] != 'c' and coord[2] != 'm':
            bad_input(field)
            continue
        coord[0] = int(coord[0])
        coord[1] = int(coord[1])
        if not coord_in_range(field, coord[0] - 1, coord[1] - 1):
            bad_input(field)
            continue
        os.system('cls' if os.name == 'nt' else 'clear')
        if not react_to_input(field, coord):
            in_game = False
            print('Game over!')


while True:
    start_game(2, 4, 4)
    if input('Do you want to restar? y/n') == 'y':
        continue
    else:
        break
